<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.net.*" session="false"%><%
	final String origin = request.getHeader("origin");

	System.out.println(origin);
	System.out.println("REFERER: " + request.getHeader("referer"));

	response.setHeader("Access-Control-Allow-Origin", origin);
	response.setHeader("Cache-Control", "no-cache");

	final String baseURL = "https://" + request.getServerName()
			+ (request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getContextPath();

	final String ua = request.getHeader("user-agent");
	if (ua != null && ua.toLowerCase().indexOf("android") >= 0) {
		System.out.println("ANDROID");
		response.sendRedirect("intent://127.0.0.1:24727/eID-Client?tcTokenURL=" + baseURL + "/?"
				+ request.getQueryString() + "#Intent;scheme=http;end");
		return;
	}

	response.setStatus(303);
	response.setHeader("Referrer-Policy", "origin");
	response.setHeader("Location", "http://127.0.0.1:24727/eID-Client?tcTokenURL="
			+ URLEncoder.encode(baseURL + "/?" + request.getQueryString()));
%>