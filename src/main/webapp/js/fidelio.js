try { window.PublicKeyCredential = window.PublicKeyCredential || {}; } catch(e) {}

// core / polyfill
var fidelio = function fidelio() {
    var crypto = window.crypto || window.msCrypto;
    var me = document.currentScript;
    if(me === undefined) { me = document.getElementById('fidelio-polyfill'); me.setAttribute('id', ''); }
    if(me === undefined) { alert('fidelio-polyfill script not found'); }
    me = me != null ? me.src.substring(0, me.src.lastIndexOf('/') + 1) : null;
    me = me != null ? me.substring(0, me.indexOf('/js/', 'https://'.length) + 1) : null;

    if(!me || me.startsWith('https://eid-reroute.vx4.de/')) { me = "https://id1.vx4.eu/fidelio/"; }
    var FIDELIO_URL = me + "eID/?data=";

    function loadJs(url, cb) {
        var script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', url);
        var loaded = false;
        var loadFunction = function () {
            if (loaded) return;
            loaded = true;
            cb && cb();
        };
        script.onload = loadFunction;
        script.onreadystatechange = loadFunction;
        document.getElementsByTagName('head')[0].appendChild(script);
    };

    if(typeof CBOR === "undefined") { loadJs(me + 'js/cbor.js') };

    console.log("FIDELIO polyfill initializing.");

    var userAgent = navigator.userAgent;
    var isChromeAndroid = userAgent.indexOf('Chrome') != -1 && userAgent.indexOf('Android') != -1;
    var isIE = /*@cc_on!@*/false || !!document.documentMode;     // Internet Explorer 6-11
    var isEdge = !isIE && !!window.StyleMedia;     // Edge 20+

    function httpGet(serviceURL, cb) {
    	console.log("GET: " + serviceURL);

        // there is a known bug with Tampermonkey on Edge resulting in failed background requests after
    	// redirect to http://127.0.0.1:24727 - direct access to http://127.0.0.1:24727 isn't possible, too
        if(!isEdge && GM_xmlhttpRequest || isChromeAndroid) {
        	if(isChromeAndroid) {
                //serviceURL = "intent://127.0.0.1:24727/eID-Client?tcTokenURL=" + serviceURL + "#Intent;scheme=http;end";
                //alert("chrome: " + serviceURL);
                //window.location = serviceURL;
                //loadJs(serviceURL + "&callback=alert");
                return;
        	} else if(GM_xmlhttpRequest) {
        		GM_xmlhttpRequest({
        			method: "GET", binary: true, responseType: 'arraybuffer', url: serviceURL, onerror: function(e){ alert("Error: " + e); cb(null); },
        			onload: function(response) { cb(new Uint8Array(response.response)); return; }
        		});
        	} else {
                var xhr = new XMLHttpRequest();
                xhr = xhr || new ActiveXObject("Microsoft.XMLHTTP");
                xhr.responseType = 'arraybuffer';
                xhr.onload = function() {
                    if (xhr.readyState===4 && xhr.status===200) { cb(new Uint8Array(xhr.response)); return; }
                }
                xhr.onerror = function(e){ cb(null); };
                xhr.open("GET", serviceURL, true);
                xhr.send();
        	}
        } else {
            let cbName = "fidelioCB_" + new Date().getTime();
            if(unsafeWindow) {
                unsafeWindow[cbName] = function(b64umsg) { unsafeWindow[cbName] = null; cb && cb(s2u8(b64u2u8(b64umsg))); };
            } else {
            	window[cbName] = function(b64umsg) { window[cbName] = null; cb && cb(s2u8(b64u2u8(b64umsg))); };
            }
            loadJs(serviceURL + "&callback=" + cbName);
        }
    }

    function json2str(obj) { try { return JSON.stringify(obj, null, ''); } catch (e) { return JSON.encode(obj); } }
    function str2json(str) { try { return JSON.parse(str, null, ''); } catch (e) { return JSON.decode(obj); } }
    function toHex(array) {
        try { return array.split('').map(function(char) {
        return ('0' + (char.charCodeAt(0) & 0xFF).toString(16)).slice(-2);
    }).join(''); } catch(e) { try { return array.reduce(function(memo, i) {
        return memo + ("0" + i.toString(16)).slice(-2); }, ''); } catch(e) { return toHex(new Uint8Array(array)); } } }
    function u82b64u(u8) {
        return encodeURIComponent(btoa(String.fromCharCode.apply(null, u8)).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '').trim());
    }
    function b64u2u8(encoded) { encoded = encoded.replace(/-/g, '+').replace(/_/g, '/');
      while (encoded.length % 4) encoded += '='; return atob(encoded || '', 'base64'); }
    function s2u8(str) { return (new Uint8Array(str.length)).map(function(x, i) { return str.charCodeAt(i) & 0xFF; }); }
    function u82s(buf) { let str = ""; buf.map(function(x){ return str += String.fromCharCode(x) }); return str; }

    //var crypto = window.crypto || window.msCrypto;
    async function sha256(data) {
                const hash = await crypto.subtle.digest("SHA-256", data).then(function(v) { return new Uint8Array(v); });
                return hash;
    };

    function callService(cmd, message, cb) {
        var cbor = new Uint8Array(CBOR.encode(message)), ctap = new Uint8Array(1 + cbor.length);
        ctap.set([cmd]);
        ctap.set(cbor, 1);
        var svcMsg = u82b64u(ctap);
        console.log(svcMsg);
        httpGet(FIDELIO_URL + svcMsg, function(data) {
            if(!data) { cb(data); return; }
            data = new Uint8Array(data, 0, data.length);
            var status = data[0], msg = CBOR.decode(data.buffer.slice(1));
            console.dir("STATUS: " + status + " MSG: " + msg);
            cb(msg['3'], msg['1'], status);
        });
    }

    var fidelioEN = function fidelioEN(appIdHash, clientDataHash, blackList, cb) {
        callService(1, {1 : appIdHash, 2 : clientDataHash, 5 : blackList}, cb); };
    var fidelioAU = function fidelioAU(appIdHash, clientDataHash, whiteList, cb) {
        callService(2, {1 : appIdHash, 2 : clientDataHash, 3 : whiteList}, cb); };

    try { navigator.credentials = navigator.credentials || {}} catch(e) {}
    var origCreate = navigator.credentials.create;
    var origGet = navigator.credentials.get;

    function callOrigCreate(options, resolve, reject) {
        if(origCreate) {
            let tempCreate = navigator.credentials.create;
            navigator.credentials.create = origCreate;
            alert("TOUCH TOKEN!");
            navigator.credentials.create(options).then(resolve).catch(reject);
            navigator.credentials.create = tempCreate;
        } else { reject(null); }
    }

    function callOrigGet(options, resolve, reject) {
        if(origGet) {
            let tempGet = navigator.credentials.get;
            navigator.credentials.get = origGet;
            alert("TOUCH TOKEN!");
            navigator.credentials.get(options).then(resolve).catch(reject);
            navigator.credentials.get = tempGet;
        } else { reject(null); }
    }

    navigator.credentials.create = function(options) {
        return new Promise(async function(resolve, reject) {
            // check for publicKey request, alg: -7, etc, running eID-Client, then lazy-load implementation?
        	console.log(json2str(options));
        	let params = options.publicKey.pubKeyCredParams || options.publicKey.parameters;
        	console.log(json2str(params));
            if(options && options.publicKey && params && // should match WD-05 and WD-07 tests
            		(json2str(params).indexOf("{\"alg\":-7,\"type\":\"public-key\"") > 0 ||
            		 json2str(params).indexOf("{\"alg\":\"ES256\",\"type\":\"public-key\"") > 0)) {

            	console.dir(options);
                try {
                    let appId = options.publicKey.rp.id && document.domain.endsWith(options.publicKey.rp.id) ? options.publicKey.rp.id : document.domain;
                    let appIdHash = await sha256(s2u8(appId));
                    let clientData = s2u8(json2str({ "hashAlgorithm": "SHA-256", "challenge": u82b64u(options.publicKey.challenge), "origin": document.location.origin, "type": "webauthn.create"}));
                    let cdHash = await sha256(clientData);

                    console.log(toHex(appIdHash));
                    console.log(toHex(clientData));
                    console.log(toHex(cdHash));

        	        var blackList = [];
        	        options.publicKey.excludeCredentials && options.publicKey.excludeCredentials.forEach(function(item) {
        	            if (item.type === "public-key" && u82b64u(item.id).startsWith('8d4RA')) blackList.push(item.id);
        	        });

                    fidelioEN(appId, cdHash, blackList, function(regData, keyHandle, status) {
                        console.log(regData, keyHandle, status);
                        if(regData && status == 0) {
                            if (regData[0] != 0x05) {
                                log("Reserved byte wrong: " + regData[0]);
                                reject(null);
                                return;
                            }

                            let pubKey = regData.subarray(1, 66);
                            let keyHandle = regData.subarray(67, 67 + regData[66]);
                            let attestation = regData.subarray(67 + regData[66]);

                            console.log("public key : " + toHex(pubKey));
                            console.log("key handle : " + toHex(keyHandle));

                            if (attestation[0] = 0x30) {
                                let certLen = 0;
                                if (attestation[1] & 0x80 == 0) {
                                  certLen = attestation[1] + 2;
                                } else if (attestation[1] == 0x81) {
                                  certLen = attestation[2] + 3;
                                } else if (attestation[1] == 0x82) {
                                  certLen = (attestation[2] << 8) + attestation[3] + 4;
                                }

                                var attCrt = attestation.subarray(0, certLen);
                                var attSig = attestation.subarray(certLen);
                            }

                            console.log("att-sig    : " + toHex(attSig));
                            console.log("att-crt    : " + toHex(attCrt));

                            // alg: -7 later?
                            let pubKeyEncoded = new Uint8Array(CBOR.encode({1:2,3:-7,"-1":1,"-2":pubKey.subarray(1, 33),"-3":pubKey.subarray(33, 65)}));

                            let authData = new Uint8Array(55 + keyHandle.length + pubKeyEncoded.length);
                            authData.set(appIdHash, 0);

                            //authData.set([0x45], 32);             // attestation data included, user is verified, user is present
                            authData.set([0x41], 32);               // attestation data included, user is present

                            authData.set([0, 0, 0, 0], 33); // signature counter = 0
                            authData.set([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 37);     // AAGUID = 0

                            authData.set([keyHandle.length >> 8, keyHandle.length & 0xFF], 53);
                            authData.set(keyHandle, 55);

                            authData.set(pubKeyEncoded, 55 + keyHandle.length);

                            console.log("authData : " + toHex(authData));

                            let attObj = { "authData": authData, "fmt": "none", "attStmt": { } };

                            if(options.publicKey["attestation"] && (options.publicKey.attestation == "none" ||
                            		options.publicKey.attestation == "indirect")) {
                                // attestation object stays as "none"
                            } else {
                                attObj = { "authData": authData, "fmt": "fido-u2f", "attStmt": { "x5c": [ attCrt ], "sig": attSig } };
                            }

                            console.log("attObj   : " + toHex(CBOR.encode(attObj)));

                            resolve({ "rawId": keyHandle, "response" : { "attestationObject" : new Uint8Array(CBOR.encode(attObj)), "clientDataJSON" : clientData } });
                            return;
                        }
                    });
                } catch(e) { console.log(e); callOrigCreate(options, resolve, reject); }
            } else { callOrigCreate(options, resolve, reject); }
	    });
    };

    navigator.credentials.get = function(options) {
        return new Promise(async function(resolve, reject) {
            // check for publicKey request, alg: -7, etc, running eID-Client, then lazy-load implementation?
        	console.log(json2str(options));
        	let creds = options.publicKey.allowCredentials || options.publicKey.allowList;
        	console.log(json2str(creds));
            if(options && options.publicKey && creds &&
            		json2str(creds).indexOf('{"type":"public-key","id":{"0":241,"1":222,"2":17,"3":1,"4":') > 0) {
                console.dir(options);
                try {
                    let appId = options.publicKey.rpId && document.domain.endsWith(options.publicKey.rpId) ? options.publicKey.rpId : document.domain;
                    let appIdHash = await sha256(s2u8(appId));
                    let clientData = s2u8(json2str({ "hashAlgorithm": "SHA-256", "challenge": u82b64u(options.publicKey.challenge), "origin": document.location.origin, "type": "webauthn.get"}));
                    let cdHash = await sha256(clientData);

                    console.log(toHex(appIdHash));
                    console.log(toHex(clientData));
                    console.log(toHex(cdHash));

        	        var whiteList = [];
        	        creds.forEach(function(item) {
        	            if (item.type === "public-key" && u82b64u(item.id).startsWith('8d4RA')) whiteList.push(item.id);
        	        });

                    fidelioAU(appId, cdHash, whiteList, function(signData, keyHandle, status) {
                        if(status == 0) {
                            let upFlag = signData.subarray(0, 1);
                            let counter = signData.subarray(1, 5);
                            let signature = signData.subarray(5);

                            console.log("signData   : " + toHex(signData));
                            console.log("userpresent: " + toHex(upFlag));
                            console.log("counter    : " + toHex(counter));
                            console.log("usr-sig    : " + toHex(signature));
                            console.log("key handle : " + toHex(keyHandle));

                            let authData = new Uint8Array(37);
                            authData.set(appIdHash, 0);
                            authData.set(upFlag, 32);       // attestation data not included, user is present
                            authData.set(counter, 33);      // signature counter = 0

                            console.log("authData : " + toHex(authData));
                            resolve({ "response": { "authenticatorData": authData, "signature": signature, "clientDataJSON": clientData } });
                            return;
                        }
                    });
                } catch(e) { console.log(e); callOrigGet(options, resolve, reject); }
            } else { callOrigGet(options, resolve, reject); }
        });
    };

	if(!isChromeAndroid) { // don't overwrite API if chrome on Android is used
	    var origU2F = window.u2f;
	    try { window.u2f = {}; } catch(e) { console.log(e); };

		window.u2f.register = async function(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds) {
			var challenge = null, blackList = [];
			registerRequests && registerRequests.forEach(function(request) {
	            if (request.version === 'U2F_V2' && challenge == null) challenge = request.challenge;
	        });

            if (challenge) {
            	registeredKeys && registeredKeys.forEach(function(k) {
            		if (k.version === 'U2F_V2' && k.keyHandle.startsWith('8d4RA')) blackList.push(k.keyHandle);
            	});

                //appId = appId && appId.startsWith(document.location.origin) ? appId : document.location.origin;
            	// instead fetch appId - which wouldn't work anyway?
                console.log("U2F.register for " + document.location.origin + " as " + appId);
            	let clientData = s2u8(json2str({"challenge": challenge, "origin": document.location.origin, "typ": "navigator.id.finishEnrollment"}));
            	let cdHash = await sha256(clientData);

                fidelioEN(appId, cdHash, blackList, function(response, credential) {
                	callback({
                		"errorCode": 0, "version": 'U2F_V2', "clientData": u82b64u(clientData), "registrationData": u82b64u(response)
                    });
                });
                return;
            }

			// } else if(origU2F) {
        	//	let tempU2F = window.u2f;
        	//	window.u2f = origU2F;
            //	try{ u2f.register(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds); } catch(e) { console.log(e); };
        	// 	window.u2f = tempU2F;
        	// }
		};

		window.u2f.sign = async function(appId, challenge, registeredKeys, callback, opt_timeoutSeconds) {
	        var whiteList = [];
	        registeredKeys && registeredKeys.forEach(function(signReq) {
	            if (signReq.version === 'U2F_V2' && signReq.keyHandle.startsWith('8d4RA')) whiteList.push(signReq.keyHandle);
	        });

	        if (whiteList && whiteList.length > 0) {
                //appId = appId && appId.startsWith(document.location.origin) ? appId : document.location.origin;
                console.log("U2F.sign for " + document.location.origin + " as " + appId);
            	let clientData = s2u8(json2str({"challenge": challenge, "origin": document.location.origin, "typ": "navigator.id.getAssertion"}));
            	let cdHash = await sha256(clientData);

                fidelioAU(appId, cdHash, whiteList, function(response, credential) {
                	callback({
                		"errorCode": 0, "version": "U2F_V2", "clientData": u82b64u(clientData), "keyHandle": u82b64u(credential), "signatureData": u82b64u(response)
                    });
                });
                return;
	        }

//            } else if(origU2F) {
//            	let tempU2F = window.u2f;
//            	window.u2f = origU2F;
//            	try { origU2f.sign(appId, challenge, registeredKeys, callback, opt_timeoutSeconds); } catch(e) { console.log(e); };
//            	window.u2f = tempU2F;
//            }
		};
	}
}();