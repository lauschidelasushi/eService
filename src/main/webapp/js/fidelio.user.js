//==UserScript==
//@name         FIDELIO Injector
//@namespace    https://id1.vx4.eu/
//@version      0.5
//@description  providing eID WebAuthn to stock browsers
//@author       ck@vx4.de
//@match        https://eid-reroute.vx4.de/*
//@match        https://id1.vx4.eu/*
//@match        https://*.bin.coffee/*
//@-match        https://*/*
//@grant        GM_xmlhttpRequest
//@connect      id1.vx4.eu
//@require      https://id1.vx4.eu/fidelio/js/cbor.js
//@require      https://id1.vx4.eu/fidelio/js/fidelio.js?7BG
//@run-at       document-start
//==/UserScript==

(function() {
 'use strict';

 // Your code here...
 try { window.PublicKeyCredential = window.PublicKeyCredential || {}; } catch(e) {}

 Object.defineProperty(window.u2f, "register", { writable: false, configurable:false, value: window.u2f.register });
 Object.defineProperty(window.u2f, "sign", { writable: false, configurable:false, value: window.u2f.sign });
 Object.defineProperty(window, "u2f", { writable: false, configurable:false, value: window.u2f });

 if(unsafeWindow) {
     Object.defineProperty(unsafeWindow, "u2f", { writable: false, configurable:false, value: window.u2f });
 }
})();
