/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.format;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.persoapp.core.util.Hex;


/**
 * Encapsulates a request to the FIDELIO eService containing a command, appID, clientData and keyHandles.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */

public class FIDELIORequest {
	private Map<?, ?> delegate;

	private final byte command;
	private final byte[] appIdHash;
	private final byte[] clientDataHash;
	private final List<byte[]> keyHandles;

	// candidate for removal
	// private final String version;


	/**
	 * Create instance from supplied command and map of input parameters.
	 *
	 * @param command
	 *            FIDO U2F command
	 * @param delegate
	 *            map containing input parameters
	 */

	// @JsonCreator
	public FIDELIORequest(final byte command, final Map<?, ?> delegate) {
		this.command = command;
		this.delegate = delegate;
		this.appIdHash = fromObject(this.delegate.get("1"));
		this.clientDataHash = fromObject(this.delegate.get("2"));
		// enrollment
		List<byte[]> kH = fromObject((Collection<?>) this.delegate.get("5"));
		if (kH == null) {
			// authenticate
			kH = fromObject((Collection<?>) this.delegate.get("3"));
		}

		this.keyHandles = kH;

		// this.version = delegate.get("version") == null ? null : delegate.get("V").toString();
	}


	/**
	 * Create instance from supplied command, application ID hash, client data hash and list of key-handles
	 *
	 * @param command
	 *            FIDO U2F command
	 * @param appIdHash
	 *            hash of application ID
	 * @param clientDataHash
	 *            hash of client data structure
	 * @param keyHandles
	 *            list of keyHandles
	 */
	public FIDELIORequest(final byte command, final byte[] appIdHash, final byte[] clientDataHash,
			final List<byte[]> keyHandles
	/* final String version */ ) {

		this.command = command;
		this.appIdHash = appIdHash;
		this.clientDataHash = clientDataHash;
		this.keyHandles = keyHandles;
		// this.version = version;
	}


	/**
	 * Return a byte-array from different supported encodings supplied as Java object. This method is especially useful
	 * to support different contents of application ID / application ID hash in case of FIDO U2F 1.x and FIDO2 /
	 * WebAuthn support.
	 *
	 * @param o
	 *            input for byte-array
	 * @return parsed representation
	 */

	private final byte[] fromObject(final Object o) {
		if (o == null) {
			return null;
		} else if (o instanceof byte[]) {
			return (byte[]) o;
		} else if (o instanceof String) {
			final String s = (String) o;

			// try the default, is it a domain name? hex and base64 strings don't contain dots
			if (s.contains(".")) {
				try {
					return MessageDigest.getInstance("SHA-256").digest(s.getBytes());
				} catch (final NoSuchAlgorithmException e) {
				}
			}

			try {
				// try decode hex encoding
				return Hex.fromString(s);
			} catch (final Exception e) {
			}

			try {
				// try decode web-safe base64
				return Base64.getUrlDecoder().decode(s);
			} catch (final Exception e) {
			}

			try {
				// try decode standard base64
				return Base64.getDecoder().decode(s);
			} catch (final Exception e) {
			}

		}
		throw new IllegalArgumentException("Type not supported: " + o.getClass() + " = " + o.toString());
	}


	/**
	 * Return a list of byte-arrays from different supported encodings supplied as collection of Java objects.
	 *
	 * @param c
	 *            input for list of byte-arrays
	 * @return list of byte-arrays containing parsed representation
	 */
	private final List<byte[]> fromObject(final Collection<?> c) {
		return c == null ? null : c.stream().map(e -> fromObject(e)).collect(Collectors.toList());
	}


	/**
	 * Returns the command value of this request.
	 *
	 * @return command
	 */
	public byte getCommand() {
		return this.command;
	}


	/**
	 * Returns the application ID hash value of this request.
	 *
	 * @return application ID hash
	 */
	public byte[] getAppIdHash() {
		return this.appIdHash;
	}


	/**
	 * Returns the client data hash value of this request.
	 *
	 * @return client data hash
	 */
	public byte[] getClientDataHash() {
		return this.clientDataHash;
	}


	/**
	 * Returns the key handle list value of this request.
	 *
	 * @return list of key handles
	 */
	public List<byte[]> getKeyHandles() {
		return this.keyHandles;
	}

	// candidate for removal
	// public String getVersion() {
	// return this.version;
	// }

	// candidate for removal
	// public Object get(final String key) {
	// return this.delegate.get(key);
	// }
}
