/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.SimpleValue;
import co.nstant.in.cbor.model.UnicodeString;


/**
 * This class provides utility functions to decode CBOR messages,
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */
public class CBORUtil {

	/**
	 * Create a (supported) object from input value <b>val</b>
	 *
	 * @param val
	 *            input value from CborDecoder
	 * @return decoded object
	 */
	public static Object from(final DataItem val) {
		if (val instanceof ByteString) {
			return ((ByteString) val).getBytes();
		} else if (val instanceof UnicodeString) {
			return ((UnicodeString) val).getString();
		} else if (val instanceof SimpleValue) {
			switch (((SimpleValue) val).getSimpleValueType().getValue()) {
			case 20:
				return false;
			case 21:
				return true;
			case 22: // null
			case 23: // undefined
			default: // unknown / reserved
				return null;
			}
		} else if (val instanceof co.nstant.in.cbor.model.UnsignedInteger) {
			return ((co.nstant.in.cbor.model.UnsignedInteger) val).getValue();
		} else if (val instanceof co.nstant.in.cbor.model.Number) {
			return ((co.nstant.in.cbor.model.Number) val).getValue();
		} else if (val instanceof co.nstant.in.cbor.model.Array) {
			return ((co.nstant.in.cbor.model.Array) val).getDataItems().stream().map(x -> from(x))
					.collect(Collectors.toList());
		} else if (val instanceof co.nstant.in.cbor.model.Map) {
			return map(val);
		} else {
			return val.toString();
		}
	}


	/**
	 * Return a java hash map representing the input value <b>val</b> with decoded (supported) object if <b>val</b> is a
	 * CBOR map.
	 *
	 * @param val
	 *            input value from CborDecoder
	 * @return map of key-value-pairs representing input
	 */
	public static Map<Object, Object> map(final DataItem val) {
		final co.nstant.in.cbor.model.Map dataItems = (co.nstant.in.cbor.model.Map) val;
		final Map<Object, Object> map = new HashMap<>();

		// convert all dataItems incl. keys to native values, no numbers -> strings
		for (final DataItem key : dataItems.getKeys()) {
			map.put(CBORUtil.from(key).toString(), CBORUtil.from(dataItems.get(key)));
		}

		return map;
	}
}
