/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.crypto;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;

import de.persoapp.core.util.ArrayTool;
import de.persoapp.core.util.TLV;


/**
 * Adopted from PersoApp PACE implementation to calculate public keys from key-handles.
 *
 * @see de.persoapp.core.card.PACE
 * @author Christian Kahlo, Ralf Wondratschek
 */
public class ECTool {

	/**
	 * The magic constant ZERO.
	 */
	private static final BigInteger ZERO = BigInteger.ZERO;

	/**
	 * The magic constant ONE.
	 */
	private static final BigInteger ONE = BigInteger.ONE;

	/**
	 * The magic constant INFINITY.
	 */
	private static final BigInteger[] INFINITY = new BigInteger[] { null, null, null };

	/** The point Q. */
	private final BigInteger Q;

	/** The point A. */
	private final BigInteger A;

	/** The precomputed curve points. */
	private final BigInteger[][] preComp;

	private SecureRandom random;
	private final BigInteger N;
	private final int nBitLength;


	/**
	 * Initializes a creation of the <em>ECDH</em> according to the given <em>ecSpec</em>.
	 *
	 * @param ecSpec
	 *            - The elliptic curve specifications.
	 */
	public ECTool(final ECParameterSpec ecSpec) {
		try {
			// getInstanceStrong() will use /dev/random, not /dev/urandom and
			// block until enough random is available
			this.random = SecureRandom.getInstance("SHA1PRNG");
		} catch (final NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
			this.random = new SecureRandom();
		}

		final EllipticCurve curve = ecSpec.getCurve();
		this.Q = ((ECFieldFp) curve.getField()).getP();
		this.A = curve.getA();

		this.N = ecSpec.getOrder();
		this.nBitLength = this.N.bitLength();

		this.preComp = new BigInteger[16][];
		final ECPoint G = ecSpec.getGenerator();
		this.preComp[0] = new BigInteger[] { G.getAffineX(), G.getAffineY(), ONE };
		preComputation(this.preComp);
	}


	/**
	 * Precomputes pcData to achieve higher calculation speed of the ECDH.
	 *
	 * @param pcData
	 *            - The data to precompute.
	 */
	private final void preComputation(final BigInteger[][] pcData) {
		final BigInteger[] Ptwice = multiplyBy2NEW(pcData[0]);
		for (int i = 1; i < pcData.length; i++) {
			pcData[i] = add(Ptwice, pcData[i - 1]);
		}
	}


	/**
	 * Deletes leading zeros.
	 *
	 * @param i
	 *            - The number which leading zeros are going to be deleted.
	 * @return returns the reduced ByteArray.
	 */
	private byte[] reduceBigInt(final BigInteger i) {
		byte[] result = i.toByteArray();
		while (result[0] == 0) {
			result = ArrayTool.subArray(result, 1, result.length - 1);
		}
		return result;
	}


	/**
	 * Encodes the point, which is identified through the parameter <em>p</em> and returns it as an byte array.
	 *
	 * @param p
	 *            - The array of BigInteger values, which needs to be encoded. Can't be <b>null</b>.
	 * @return The encoded point as an byte array.
	 */
	private byte[] encodePoint(final BigInteger[] p) {
		return ArrayTool
				.arrayconcat(new byte[] { 0x04 }, ArrayTool.arrayconcat(reduceBigInt(p[0]), reduceBigInt(p[1])));
	}


	/**
	 * @param privKey
	 *            - the private key
	 * @return returns the public key
	 */
	public final byte[] toPublicKey(final BigInteger privKey) {
		return encodePoint(fastMultiply(privKey));
	}


	/**
	 * Create an ECDSA signature of hash using the supplied key
	 *
	 * @param key
	 *            private key to be used for signing operation
	 * @param hash
	 *            hash of data to be signed
	 * @return
	 */
	public final byte[] sign(final BigInteger key, final byte[] hash) {
		BigInteger e = new BigInteger(1, hash);
		if (hash.length * 8 - this.nBitLength > 0) {
			e = e.shiftRight(hash.length * 8 - this.nBitLength);
		}

		BigInteger k, r;
		do { // r = x-axis of G'[x, y, z] = G[x, y, z] * k
			do { // k
				k = new BigInteger(this.nBitLength, this.random);
			} while (ZERO.equals(k) || k.compareTo(this.N) >= 0 || !k.gcd(this.N).equals(ONE));
			r = fastMultiply(k)[0];
		} while (ZERO.equals(r));

		// (r*key + e) / k = (P[x] * key + e) / k = ((k * G)[x] * key + e) / k
		final BigInteger s = key.multiply(r).add(e).multiply(k.modInverse(this.N)).mod(this.N);

		return TLV.build(0x30, ArrayTool.arrayconcat(TLV.build(0x02, r.toByteArray()),
				TLV.build(0x02, TLV.concat(s.toByteArray()))));
	}


	/*
	 * The following implementation uses modified Jacobian coordinates somewhat inspired from
	 * http://point-at-infinity.org/ecc/Prime_Curve_Standard_Projective_Coordinates.html but without returning aZ'^4, a
	 * point is a therefore a triple, not a quadruple. aZ^4 is calculated because this couple of multiplications turned
	 * out to be quite fast.
	 */

	/**
	 * Determines the non adjacent form. The non-adjacent form (NAF) of a number is a unique signed-digit
	 * representation. Like the name suggests, non-zero values cannot be adjacent.
	 *
	 * @param e
	 *            - The number, that is to be converted.
	 * @param w
	 *            - The entries <em>n</em> of the Naf are smaller than 2^<em>w</em>.
	 * @param b
	 *            - The BitLength of e.
	 * @return Returns the non adjacent form
	 */
	private static int[] determineNaf(final BigInteger e, final int w, final int b) {
		final int power2wi = 1 << w;
		int j, u;
		final int[] N = new int[b + 1];
		BigInteger c = e.abs();
		final int s = e.signum();

		j = 0;
		while (c.compareTo(BigInteger.ZERO) > 0) {
			if (c.testBit(0)) {
				u = c.intValue() & (power2wi << 1) - 1;
				if ((u & power2wi) != 0) {
					u = u - (power2wi << 1);
				}

				c = c.subtract(BigInteger.valueOf(u));
			} else {
				u = 0;
			}

			N[j++] = s > 0 ? u : -u;
			c = c.shiftRight(1);
		}

		// fill with zeros
		while (j <= b) {
			N[j++] = 0;
		}

		return N;
	}


	/**
	 * Multiplies the parameter <em>k</em> with the Naf determined by the function determineNaf. Returns the result
	 * after the multiplication has finished.
	 *
	 * @param k
	 *            - The value, to multiply.
	 * @return Returns the Result as a array of BigInteger values.
	 */
	public BigInteger[] fastMultiply(final BigInteger k) {
		final int[] N = determineNaf(k, 5, k.bitLength());

		BigInteger[] r = INFINITY;
		final int l = N.length - 1;
		for (int i = l; i >= 0; i--) {
			r = multiplyBy2NEW(r);
			final int index = N[i];
			if (index > 0) {
				r = add(r, this.preComp[index - 1 >> 1]);
			} else if (index < 0) {
				r = subtract(r, this.preComp[-index - 1 >> 1]);
			}
		}

		BigInteger x = r[0], y = r[1], z = r[2];

		z = z.modInverse(this.Q);
		final BigInteger z2 = z.multiply(z);
		z = z.multiply(z2);
		x = x.multiply(z2);
		y = y.multiply(z);

		// coordinates are affine, that's why Z = 1
		return new BigInteger[] { x.mod(this.Q), y.mod(this.Q), ONE };
	}


	/**
	 * <p>
	 * Adds the two points of a Cartesian-Coordinate system to each other and returns the result.
	 * </p>
	 *
	 * @param a
	 *            - The first point.
	 * @param b
	 *            - The second point.
	 * @return The calculated point.
	 */
	private BigInteger[] add(final BigInteger[] a, final BigInteger[] b) {
		if (a[0] == null && a[1] == null) {
			return b;
		}
		if (b[0] == null && b[1] == null) {
			return a;
		}

		final BigInteger aX = a[0], aY = a[1], aZ = a[2];
		final BigInteger bX = b[0], bY = b[1], bZ = b[2];

		BigInteger U1, S1, U2, S2;

		// point B is affine
		if (bZ.equals(ONE)) {
			// U1 = aX*bZ^2
			U1 = aX;
			// S1 = aY*bZ^3
			S1 = aY;
		} else {
			final BigInteger bZ2 = bZ.multiply(bZ).mod(this.Q);
			// U1 = aX*bZ^2
			U1 = aX.multiply(bZ2).mod(this.Q);
			// S1 = aY*bZ^3
			S1 = aY.multiply(bZ2.multiply(bZ).mod(this.Q)).mod(this.Q);
		}

		// point A is affine
		if (aZ.equals(ONE)) {
			// U2 = bX*aZ^2
			U2 = bX;
			// S2 = bY*aZ^3
			S2 = bY;
		} else {
			final BigInteger aZ2 = aZ.multiply(aZ).mod(this.Q);
			// U2 = bX*aZ^2
			U2 = bX.multiply(aZ2).mod(this.Q);
			// S2 = bY*aZ^3
			S2 = bY.multiply(aZ2.multiply(aZ).mod(this.Q)).mod(this.Q);
		}

		// H = bX*aZ^2 - aX*bZ^2
		final BigInteger H = U2.subtract(U1);

		// r = bY*aZ^3 - aY*bZ^3
		final BigInteger r = S2.subtract(S1);
		if (H.equals(ZERO)) {
			if (r.equals(ZERO)) {
				return multiplyBy2NEW(a);
			}
			return INFINITY;
		}

		// U2 = (bX*aZ^2 - aX*bZ^2)^2
		U2 = H.multiply(H).mod(this.Q);

		// S2 = (bX*aZ^2 - aX*bZ^2)^3
		S2 = U2.multiply(H).mod(this.Q);

		// U2 = U1H^2
		U2 = U2.multiply(U1).mod(this.Q);

		// x = r^2 - S2 - 2U2
		final BigInteger x = r.multiply(r).mod(this.Q).subtract(S2).subtract(U2.add(U2));

		// y = r(U2 - x) - S1S2
		final BigInteger y = r.multiply(U2.subtract(x)).mod(this.Q).subtract(S1.multiply(S2).mod(this.Q));

		BigInteger z;

		// z = Z1Z2H
		// point A is affine
		if (aZ.equals(ONE)) {
			// point B is not affine
			if (!bZ.equals(ONE)) {
				z = bZ.multiply(H);
			} else {
				// both are affine
				z = H;
			}
		} else if (!bZ.equals(ONE)) {
			U1 = aZ.multiply(bZ).mod(this.Q);
			z = U1.multiply(H);
		} else {
			z = aZ.multiply(H);
		}

		return new BigInteger[] { x.mod(this.Q), y.mod(this.Q), z.mod(this.Q) };
	}


	/**
	 * <p>
	 * Subtracts two points of an Cartesian-Coordinate system from each other and returns the result.
	 * </p>
	 *
	 * @param a
	 *            - The first point.
	 * @param b
	 *            - The second point.
	 * @return The calculated point.
	 */
	private BigInteger[] subtract(final BigInteger[] a, final BigInteger[] b) {
		return add(a, new BigInteger[] { b[0], b[1].negate(), b[2] });
	}


	/**
	 * Calculates the double of the given point
	 *
	 * @param p
	 *            - The point, to double.
	 * @return Returns the doubled point.
	 */
	public BigInteger[] multiplyBy2NEW(final BigInteger[] p) {
		final BigInteger pX = p[0], pY = p[1], pZ = p[2];

		if (pX == null && pY == null) { // is infinity
			// Twice identity element (point at infinity) is identity
			return p;
		}

		if (pY.signum() == 0) {
			// if y1 == 0, then (x1, y1) == (x1, -y1)
			// and hence this = -this and thus 2(x1, y1) == infinity
			return INFINITY; // that's infinity
		}

		// z = Y^2
		BigInteger z = pY.multiply(pY).mod(this.Q);

		// S = 4XY^2
		BigInteger S = pX.multiply(z).mod(this.Q);
		S = S.add(S);
		S = S.add(S);

		// M = 3X^2 + a(Z^2)^2
		BigInteger pAZ4 = this.A;
		// point is not affine
		if (!pZ.equals(ONE)) {
			pAZ4 = pZ.multiply(pZ).mod(this.Q);
			pAZ4 = this.A.multiply(pAZ4.multiply(pAZ4).mod(this.Q)).mod(this.Q);
		}

		BigInteger y = pX.multiply(pX).mod(this.Q);

		final BigInteger M = y.add(y).add(y).add(pAZ4); // 3X^2+aZ^4

		// T = x = -2S + M^2
		final BigInteger x = M.multiply(M).mod(this.Q).subtract(S.add(S));

		// y = -8Y^4 + M(S - T)
		y = z.multiply(z).mod(this.Q);
		BigInteger U = y.add(y); // 2Y^4
		z = U.add(U);
		U = z.add(z); // 8Y^4
		y = M.multiply(S.subtract(x)).mod(this.Q).subtract(U);

		// Z = 2Y
		if (!pZ.equals(ONE)) {
			z = pY.multiply(pZ).mod(this.Q);
		} else {
			z = pY;
		}
		z = z.add(z);

		return new BigInteger[] { x.mod(this.Q), y.mod(this.Q), z.mod(this.Q) };
	}
}
