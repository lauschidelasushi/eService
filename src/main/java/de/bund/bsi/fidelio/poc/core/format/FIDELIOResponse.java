/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.format;

/**
 * Encapsulates a request to the FIDELIO eService containing a status, raw response and associated key handle.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */
public class FIDELIOResponse {
	private final byte status;
	private final byte[] response;
	private final byte[] keyHandle;


	/**
	 * Create an instance made up of status, response and key handle.
	 *
	 * @param status
	 * @param response
	 * @param keyHandle
	 */
	public FIDELIOResponse(final byte status, final byte[] response, final byte[] keyHandle) {
		this.status = status;
		this.response = response;
		this.keyHandle = keyHandle;
	}


	/**
	 * Return the status value of this response.
	 *
	 * @return the status
	 */
	public byte getStatus() {
		return this.status;
	}


	/**
	 * Return the response value of this response.
	 *
	 * @return raw response value
	 */
	public byte[] getResponse() {
		return this.response;
	}


	/**
	 * Return the key handle value of this response.
	 *
	 * @return key handle
	 */
	public byte[] getKeyHandle() {
		return this.keyHandle;
	}
}
