/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import javax.crypto.Mac;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.bund.bsi.fidelio.poc.core.crypto.CryptoProfile;
import de.bund.bsi.fidelio.poc.core.crypto.FIDELIOSecretKey;
import de.persoapp.core.util.Hex;
import net.vx4.lib.eid.sfw.EIDResult;
import net.vx4.lib.eid.sfw.api.EIDGlobals.Attribute;
import net.vx4.lib.eid.sfw.api.SPSession;
import net.vx4.lib.eid.sfw.api.SPSessionMgr;


/**
 * This class provides a token key service implementation making use of the eID functionality of German national
 * identity documents to gather key-material based on the restricted identity.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */

public class TokenKeyServiceImpl implements TokenKeyService {

	private final SPSessionMgr spsm;
	private final CryptoProfile cp;
	private final byte[] eServiceSecret;


	/**
	 * Create an instance of this token key service implementation using the supplied properties <b>serviceProps</b>.
	 *
	 * @param serviceProps
	 *            properties for this token key service
	 * @throws GeneralSecurityException
	 */
	public TokenKeyServiceImpl(final Properties serviceProps) throws GeneralSecurityException {
		this.spsm = new SPSessionMgr(serviceProps);
		this.cp = new CryptoProfile(CryptoProfile.Type.P256);
		this.eServiceSecret = Hex.fromString(serviceProps.getProperty("eServiceSecret"));
	}


	/*
	 * (non-Javadoc)
	 * @see TokenKeyService#create(String)
	 */

	@Override
	public Context create(final String realm) {
		return create(realm, new Attribute[] { Attribute.RestrictedID });
	};


	/**
	 * Create a token key service context with a realm and implementation specific attributes.
	 *
	 * @param realm
	 *            description of context
	 * @param attributes
	 *            attributes required to process this context, usually {@value Attribute#RestrictedID}
	 * @return token key service context
	 */

	public Context create(final String realm, final Attribute... attributes) {
		return create(realm, (Object) attributes);
	};


	/*
	 * (non-Javadoc)
	 * @see TokenKeyService#create(String, HttpServletRequest)
	 */

	@Override
	public Context create(final String realm, final HttpServletRequest request) {
		return create(realm, (Object) request);
	};


	/**
	 * Create a token key service context with a realm and implementation specific context information.
	 *
	 * @param realm
	 *            description of context
	 * @param ctxInfo
	 *            content information required to process this context
	 * @return token key service context
	 */

	private Context create(final String realm, final Object ctxInfo) {
		// System.out.println("ctxInfo: " + ctxInfo);
		if (ctxInfo != null) {
			if (ctxInfo instanceof Attribute[]) {
				// for eID connections to eID servers with restricted ID support
				return new Context() {
					final SPSession s = TokenKeyServiceImpl.this.spsm.create();
					final byte[] appIdHash = new byte[32];
					final transient AtomicReference<Function<byte[], FIDELIOSecretKey>> fKDK = new AtomicReference<Function<byte[], FIDELIOSecretKey>>();


					/*
					 * (non-Javadoc)
					 * @see Context#connect(byte[], HttpServletResponse, String)
					 */

					@Override
					public Object connect(final byte[] appIdHash, final HttpServletResponse response,
							final String reentryUrl) throws IOException {
						put(Context.class.getName(), this); // save context object

						// create a copy of application ID hash
						System.arraycopy(appIdHash, 0, this.appIdHash, 0, this.appIdHash.length);

						// establish eID session and send TCToken data back
						this.s.to(reentryUrl, response.getWriter(), realm, (Attribute[]) ctxInfo);
						return null;
					}


					/*
					 * (non-Javadoc)
					 * @see Context#setDiv(Function<byte[], byte[]>)
					 */

					@Override
					public void setDiv(final Function<byte[], byte[]> kdf) {
						// create function of key derivation key
						final Mac fKDK = TokenKeyServiceImpl.this.cp
								.mac(new FIDELIOSecretKey(() -> kdf.apply(this.appIdHash)));
						// set function to transient atomic reference
						this.fKDK.set((x) -> new FIDELIOSecretKey(() -> fKDK.doFinal(x)));
					}


					/*
					 * (non-Javadoc)
					 * @see Context#fKX(short, byte[]...)
					 */

					@Override
					public FIDELIOSecretKey fKX(final short ctr, final byte[]... divData) {
						final ByteArrayOutputStream tbsData = new ByteArrayOutputStream();
						try {
							// build derivation data block
							tbsData.write(new byte[] { (byte) (ctr >> 8), (byte) ctr, (byte) 0x00 });
							if (divData != null) {
								for (final byte[] b : divData) {
									tbsData.write(b);
								}
							}
							tbsData.write(new byte[] { (byte) 0xFF });
							tbsData.close();
						} catch (final IOException e) {
							e.printStackTrace();
						}
						// apply key derivation key function and return result
						return this.fKDK.get().apply(tbsData.toByteArray());
					}


					/*
					 * (non-Javadoc)
					 * @see Context#get(String)
					 */

					@Override
					public Object get(final String key) {
						return this.s.get(key); // retrieve attribute from eID session
					}


					/*
					 * (non-Javadoc)
					 * @see Context#put(String, value)
					 */

					@Override
					public void put(final String key, final Object value) {
						this.s.put(key, value); // store attribute to eID session
					}
				};
			} else if (ctxInfo instanceof HttpServletRequest) {
				// i.e. for client SSL authentication
			} else {
				// other types unsupported for now
			}
		} else {
			// no context information, unsupported for now
		}

		throw new IllegalArgumentException("ctxInfo: " + ctxInfo);
	};


	/**
	 * Finishes and returns the context with the identifier <b>id</b>
	 *
	 * @param id
	 *            the context identifier
	 * @return null if the session does not exist, no result is available, resultCode is not RESULT_OK or RID is empty.
	 *         Otherwise {@link TokenKeyServiceImpl.Context} is returned.
	 */
	@Override
	public Context finish(final String id) {
		final SPSession s = this.spsm.remove(id); // retrieve service provider session from eID stack
		final EIDResult<?> result = s == null ? null : s.getResult(); // retrieve eID result
		if (result != null && EIDResult.RESULT_OK.equals(result.getResultCode())) { // check result code
			final byte[] rid = result.getRID(); // retrieve RID
			if (rid != null) { // if RID was returned process it
				// derive restricted identifier
				final Mac kdf = this.cp.mac(new FIDELIOSecretKey(() -> this.cp.mac(this.cp.hash(rid), this.eServiceSecret)));
				Arrays.fill(rid, (byte) 0); // zeroize original array

				final Context ctx = (Context) s.get(Context.class.getName()); // get context object
				if (ctx != null) {
					ctx.setDiv((divData) -> kdf.doFinal(divData)); // set diversification function
				}

				return ctx; // return context to caller
			}
		}
		return null;
	}
}
