/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.crypto;

import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


/**
 * This class provides cryptographic primitives to implement FIDO U2F token operations. The instantiated algorithms
 * depend on a "profile" reflecting the output bit length of hashes and elliptic curve key sizes.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */
public class CryptoProfile {

	private final SecureRandom random = new SecureRandom();
	private final String hashAlg;
	private final String macAlg;
	private final ECTool ecTool;


	/**
	 * This enumeration determines the profile types and associated algorithms.
	 *
	 * @author Christian Kahlo, 2017
	 * @version $Id$
	 */
	public enum Type {
		P256("SHA-256", "HmacSHA256"), P384("SHA-384", "HmacSHA384"), P512("SHA-512", "HmacSHA512");

		private final String hashAlg;
		private final String macAlg;


		private Type(final String hashAlg, final String macAlg) {
			this.hashAlg = hashAlg;
			this.macAlg = macAlg;
		}
	}


	/**
	 * Create an instance based on a cryptographic profile identifier.
	 *
	 * @param profile
	 *            the cryptographic profile to be used
	 * @throws GeneralSecurityException
	 */
	public CryptoProfile(final Type profile) throws GeneralSecurityException {
		this.hashAlg = profile.hashAlg;
		this.macAlg = profile.macAlg;

		final AlgorithmParameters algoParameters = AlgorithmParameters.getInstance("EC");
		algoParameters.init(new ECGenParameterSpec("secp256r1"));
		final ECParameterSpec parameterSpec = algoParameters.getParameterSpec(java.security.spec.ECParameterSpec.class);

		this.ecTool = new ECTool(parameterSpec);
	}


	/**
	 * Static helper function turning the GeneralSecurityException of the constructor into a RuntimeException.
	 *
	 * @param profile
	 * @return an instance of this class
	 * @throws RuntimeException
	 */
	public static CryptoProfile getInstance(final Type profile) {
		try {
			return new CryptoProfile(profile);
		} catch (final GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * Create and return a byte-array of size n and fill it with random bytes.
	 *
	 * @param n
	 *            the length of the resulting byte-array
	 * @return byte-array containing random data
	 */

	public byte[] rand(final int n) {
		final byte[] buffer = new byte[n];
		this.random.nextBytes(buffer);
		return buffer;
	}


	/**
	 * Return the hash of the supplied data arguments in declared order.
	 *
	 * @param data
	 *            input to hash function
	 * @return the resulting hash
	 */

	public byte[] hash(final byte[]... data) {
		try {
			final MessageDigest md = MessageDigest.getInstance(this.hashAlg);
			Arrays.asList(data).forEach(x -> md.update(x));
			return md.digest();
		} catch (final NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * Return the HMAC of the supplied data arguments in declared order using key as the HMAC secret.
	 *
	 * @param key
	 *            the HMAC secret
	 * @param data
	 *            input to hash function
	 * @return the resulting MAC
	 */
	public byte[] mac(final SecretKey key, final byte[]... data) {
		try {
			final Mac mac = Mac.getInstance(this.macAlg);
			mac.init(key);
			Arrays.asList(data).forEach(x -> mac.update(x));
			return mac.doFinal();
		} catch (final GeneralSecurityException e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * Return the HMAC of the supplied data arguments in declared order using key as the HMAC secret.
	 *
	 * @param key
	 *            the HMAC secret as plain byte-array
	 * @param data
	 *            input to hash function
	 * @return the resulting MAC
	 */
	public byte[] mac(final byte[] key, final byte[]... data) {
		return mac(new SecretKeySpec(key, this.macAlg), data);
	}


	/**
	 * Return an initialized ready-to-use MAC instance using key as MAC secret.
	 *
	 * @param key
	 *            the HMAC secret
	 * @return the MAC function
	 */
	public Mac mac(final SecretKey key) {
		try {
			final Mac mac = Mac.getInstance(this.macAlg);
			mac.init(key);
			return mac;
		} catch (final GeneralSecurityException e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * Return the HMAC of the data byte-array within supplied offset and length using key as the HMAC secret.
	 *
	 * @param key
	 *            the HMAC secret as plain byte-array
	 * @param data
	 *            the data to be hashed
	 * @param offset
	 *            start offset in data byte-array
	 * @param length
	 *            length beginning at offset in data byte-array
	 * @return the resulting MAC
	 */
	public byte[] mac(final byte[] key, final byte[] data, final int offset, final int length) {
		try {
			final Mac mac = Mac.getInstance(this.macAlg);
			mac.init(new SecretKeySpec(key, this.macAlg));
			mac.update(data, offset, length);
			return mac.doFinal();
		} catch (final GeneralSecurityException e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * Return the public key Q according to supplied private key.
	 *
	 * @param k
	 * @return
	 */
	public byte[] pubKey(final ECPrivateKey key) {
		return this.ecTool.toPublicKey(key.getS());
	}


	/**
	 * Return signature of signedData using supplied key.
	 *
	 * @param key
	 *            key to be used for signature
	 * @param signedData
	 *            data to be hashed and signed
	 * @return the TLV encoded signature as byte-array
	 * @throws GeneralSecurityException
	 */
	public byte[] sign(final ECPrivateKey key, final byte[] signedData) throws GeneralSecurityException {
		return this.ecTool.sign(key.getS(), hash(signedData));
	}
}
